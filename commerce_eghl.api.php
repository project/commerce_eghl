<?php

/**
 * @file
 * Hooks provided by the Commerce eGHL module.
 */

/**
 * Alter payment data before it is send with a POST request to eGHL.
 *
 * @param array &$data
 *   The payment data that can be altered.
 * @param EntityDrupalWrapper $order_wrapper
 *   The commerce order this payment is for.
 *
 * @see commerce_eghl_redirect_form()
 */
function hook_commerce_eghl_payment_alter(array &$data, EntityDrupalWrapper $order_wrapper) {
  $customer_profile = $order_wrapper->commerce_customer_billing;
  $customer_name = $customer_profile->field_customer_firstname->value() . ' '
    . $customer_profile->field_customer_lastname->value();
  $data['CustName'] = $customer_name;
}
