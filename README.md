# Commerce eGHL

This module is a payment gateway for Drupal Commerce. It provides an off-site
redirect payment method to eGHL: http://e-ghl.com/v2/ . The payment transaction
workflow was inspired by https://github.com/dilab/omnipay-eghl .

## Requirements

* PHP 5.6 or higher
* Drupal Commerce https://www.drupal.org/project/commerce

No additional eGHL PHP library or similar is required.

## Installation

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/7/extend/installing-modules
for further information.

## Configuration

Enable the module Commerce Payment UI and create a payment rule at
/admin/commerce/config/payment-methods .
Set merchant ID and Merchant password in the payment action.

Drupal Commerce Payment docs: https://docs.drupalcommerce.org/commerce1/user-guide/payments
